import datetime
import pyvista
from pyvista import themes
import sys

if __name__ == "__main__":
    # print(f"Arguments count: {len(sys.argv)}")
    # for i, arg in enumerate(sys.argv):
    #     print(f"Argument {i:>6}: {arg}")

    temp_theme = themes.DocumentTheme()
    temp_theme.cmap = 'jet'

    filename = sys.argv[1]
    print(filename)
    reader = pyvista.get_reader(filename)
    print(reader)
    reader.disable_all_point_arrays()
    reader.enable_point_array('T')
    print(reader.point_array_names)
    mesh = reader.read()
    print(mesh)
    
    pngFilename = f"{sys.argv[2]}/plot_vtk_foam_result_{datetime.datetime.now().strftime('%Y%m%d-%H%M%S_%f')}.png"
    print(pngFilename)
    mesh.plot(
        scalars=mesh.get_array('T'),
        component=mesh.get_array('T'),
        cpos='XY', 
        theme=temp_theme,
        screenshot=pngFilename)
    print(f"Max temp: {max(mesh.get_array('T'))}")